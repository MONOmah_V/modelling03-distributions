__author__ = 'monomah'

import ui_form

import distributions.uniform
import distributions.exponential

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import matplotlib
matplotlib.use('Qt5Agg')  # FIXIT
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg


class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = ui_form.Ui_Form()
        self.ui.setupUi(self)
        self._setup_validators()

        self.__test()
        self.figureUniform = plt.figure(facecolor=(0, 0, 0, 0))
        self.canvasUniform = FigureCanvasQTAgg(self.figureUniform)
        self.ui.verticalLayoutUniform.addWidget(self.canvasUniform)
        self.canvasUniform.setMinimumSize(500, 500)
        self.figureExponential = plt.figure(facecolor=(0, 0, 0, 0))
        self.canvasExponential = FigureCanvasQTAgg(self.figureExponential)
        self.ui.verticalLayoutExponential.addWidget(self.canvasExponential)
        self.canvasExponential.setMinimumSize(500, 500)

    def _setup_validators(self):
        self.validator = QDoubleValidator()
        self.intValidator = QIntValidator(100, 9999)
        #self.ui.lineEditExponentialLambda.setValidator(self.validator)
        self.ui.lineEditExponentialX0.setValidator(self.validator)
        self.ui.lineEditExponentialXn.setValidator(self.validator)
        self.ui.lineEditExponentialCount.setValidator(self.intValidator)
        self.ui.lineEditUniformA.setValidator(self.validator)
        self.ui.lineEditUniformB.setValidator(self.validator)
        self.ui.lineEditUniformX0.setValidator(self.validator)
        self.ui.lineEditUniformXn.setValidator(self.validator)
        self.ui.lineEditUniformCount.setValidator(self.intValidator)

    @pyqtSlot()
    def on_pushButtonUniform_clicked(self):
        a = float(self.ui.lineEditUniformA.text())
        b = float(self.ui.lineEditUniformB.text())
        x0 = float(self.ui.lineEditUniformX0.text())
        xn = float(self.ui.lineEditUniformXn.text())
        count = float(self.ui.lineEditUniformCount.text())
        distr = distributions.uniform.UniformDistribution(a, b, x0, xn, count)

        self.figureUniform.clf()
        cumPlot = self.figureUniform.add_subplot(2, 1, 1)
        cumPlot.plot(*distr.cumulative_distribution_theory(), color='blue', linestyle='solid')
        cumPlot.plot(*distr.cumulative_distribution_practice(), color='red', linestyle='solid')
        cumPlot.set_ylabel('Функция распределения')
        pdPlot = self.figureUniform.add_subplot(2, 1, 2)
        pdPlot.plot(*distr.propability_density_function_practice(), color='red', linestyle='none',
                    marker='.')
        pdPlot.plot(*distr.propability_density_function_theory(), color='blue', linestyle='solid')
        pdPlot.set_ylabel('Плотность вероятности')
        pdPlot.set_xlabel('x')
        self.figureUniform.suptitle("Mt[X] = {0:0.4}, Me[X] = {1:0.4}\nDt[X] = {2:0.4}, De[X] = {3:0.4}"
                                    .format(distr.expected_value_theory(), distr.expected_value_practice(),
                                            distr.variance_theory(), distr.variance_practice()))
        self.canvasUniform.draw()

    @pyqtSlot()
    def on_pushButtonExponential_clicked(self):
        lamb = float(self.ui.lineEditExponentialLambda.text())
        x0 = float(self.ui.lineEditExponentialX0.text())
        xn = float(self.ui.lineEditExponentialXn.text())
        count = float(self.ui.lineEditExponentialCount.text())
        distr = distributions.exponential.ExponentialDistribution(lamb, x0, xn, count)

        self.figureExponential.clf()
        cumPlot = self.figureExponential.add_subplot(2, 1, 1)
        cumPlot.plot(*distr.cumulative_distribution_theory(), color='blue', linestyle='solid')
        cumPlot.plot(*distr.cumulative_distribution_practice(), color='red', linestyle='solid')
        cumPlot.set_ylabel('Функция распределения')
        pdPlot = self.figureExponential.add_subplot(2, 1, 2)
        pdPlot.plot(*distr.propability_density_function_practice(), color='red', linestyle='none',
                    marker='.')
        pdPlot.plot(*distr.propability_density_function_theory(), color='blue', linestyle='solid')
        pdPlot.set_ylabel('Плотность вероятности')
        pdPlot.set_xlabel('x')
        self.figureExponential.suptitle("Mt[X] = {0:0.4}, Me[X] = {1:0.4}\nDt[X] = {2:0.4}, De[X] = {3:0.4}"
                                        .format(distr.expected_value_theory(), distr.expected_value_practice(),
                                                distr.variance_theory(), distr.variance_practice()))
        self.canvasExponential.draw()

    def __generate_range(self, x0, xn):
        step = (xn - x0) / 100.
        res = []
        while x0 < xn:
            res.append(x0)
            x0 += step
        return res

    def __test(self):
        self.ui.lineEditUniformA.setText('5')
        self.ui.lineEditUniformB.setText('15')
        self.ui.lineEditUniformX0.setText('0')
        self.ui.lineEditUniformXn.setText('20')
        self.ui.lineEditUniformCount.setText('1000')

        self.ui.lineEditExponentialLambda.setText('1')
        self.ui.lineEditExponentialX0.setText('0')
        self.ui.lineEditExponentialXn.setText('5')
        self.ui.lineEditExponentialCount.setText('1000')


def main():
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
