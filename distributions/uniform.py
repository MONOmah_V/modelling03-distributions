__author__ = 'monomah'

import numpy as np
from math import log2
from distributions.distribution_base import DistributionBase


class UniformDistribution(DistributionBase):
    def __init__(self, a, b, x0, xn, count=100):
        super().__init__(x0, xn, count)
        self.a = a
        self.b = b
        self.x0 = x0
        self.xn = xn
        self.count = count
        self.data = np.random.uniform(self.a, self.b, [self.count])
        self.data.sort()

    def propability_density_function_theory(self):
        left, right = self.__bounds()
        a = self.a
        b = self.b
        return [[left, a, a,           b,           b, right],
                [0,    0, 1 / (b - a), 1 / (b - a), 0, 0]]

    def cumulative_distribution_theory(self):
        left, right = self.__bounds()
        a = self.a
        b = self.b
        return [[left, a, b,   right],
                [0,    0, 1.0, 1.0]]

    def expected_value_theory(self):
        return (self.a + self.b) / 2

    def variance_theory(self):
        return (self.b - self.a) ** 2 / 12

    def __bounds(self):
        bound = (self.b - self.a) / 3
        return self.a - bound, self.b + bound
