__author__ = 'monomah'

import numpy as np
from math import exp, log
from distributions.distribution_base import DistributionBase


class ExponentialDistribution(DistributionBase):
    def __init__(self, lamb, x0, xn, count=100):
        super().__init__(x0, xn, count)
        self.lamb = lamb
        self.x0 = x0
        self.xn = xn
        self.count = count
        self.data = np.random.exponential(1 / self.lamb, [self.count])
        self.data.sort()
        self.xs = np.linspace(self.x0, self.xn, self.count)

    def propability_density_function_theory(self):
        return [self.xs, np.array([0 if x < 0 else self.lamb * exp(-self.lamb * x) for x in self.xs])]

    def cumulative_distribution_theory(self):
        return [self.xs, np.array([0 if x < 0 else 1 - exp(-self.lamb * x) for x in self.xs])]

    def expected_value_theory(self):
        return 1 / self.lamb

    def variance_theory(self):
        return 1 / self.lamb ** 2