__author__ = 'monomah'

import numpy as np


class DistributionBase:
    """
    Подразумевается, что self.data генерируется на иксах [self.x0, self.xn) в количестве self.count
    Например, self.data = numpy.random.uniform(self.a, self.b, [self.count])
    """
    def __init__(self, x0, xn, count):
        self.x0 = x0
        self.xn = xn
        self.count = count
        self.data = []  # Возможно, быдлокот и надо делать иначе

    def cumulative_distribution_practice(self):
        """
        Функция распределения случайной величины
        :return:
        """
        xs = np.linspace(self.x0, self.xn, self.count)
        ys = np.array([self.__calculate_distribution(x) for x in xs])
        return [xs, ys]

    def __calculate_distribution(self, x):
        res = 0
        for item in self.data:
            if item < x and res <= self.count:
                res += 1
            else:
                break
        return res / self.count

    def propability_density_function_practice(self):
        """
        Плотность вероятности
        :return:
        """
        delta = (self.xn - self.x0) / self.count
        xs = np.linspace(self.x0, self.xn, self.count)
        ys = np.array([self.__calculate_density(x, delta) for x in xs])
        return [xs, ys]

    def __calculate_density(self, x, delta):
        """
        Тут адский Израиль, хорошо бы это дело переписать по-питоновски или вообще заюзать scipy, но пять утра
        :param x:
        :param delta:
        :return: чушь
        """
        if abs(x - self.x0) < delta / 2:
            x1 = x
            x2 = x + delta
        elif abs(x - self.xn) < delta / 2:
            x1 = x - delta
            x2 = x
        else:
            x1 = x - delta / 2
            x2 = x + delta /2
        cnt1 = 0
        for item in self.data:
            if item >= x1 and item < x2:
                cnt1 += 1
        return (cnt1) / (self.count * delta)

    def expected_value_practice(self):
        """
        Математическое ожидание
        :return:
        """
        return self.data.sum() / self.count

    def variance_practice(self):
        """
        Дисперсия
        :return:
        """
        res = 0
        expected_value = self.expected_value_practice()
        for item in self.data:
            res += (item - expected_value) ** 2
        return res / (self.count - 1)
